<?php


namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class ShoppingCardProduct extends Model
{
    use SoftDeletes;

    protected $table = "shopping_card_product";

    protected $fillable = ['id', 'sepet_id', 'urun_id','quantity','price','status','created_at','updated_at','deleted_at'];


    public function product()
    {
        return $this->belongsTo('App\Product');
    }


    public function scpblgtosc()
    {
        return $this->belongsTo('App\ShoppingCard');
    }

}
