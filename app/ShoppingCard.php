<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class ShoppingCard extends Model
{
    protected $table = "shopping_card";

    protected $fillable = ['id', 'user_id', 'created_at','updated_at'];

    public function shoppingCardProducts()
    {
        return $this->hasMany('App\ShoppingCardProduct');
    }

  //  public static function active_card_id()
  //  {
  //      $active_card = DB::table('shopping_card as s')
   //         ->leftJoin('orders as si', 'si.sepet_id', '=', 's.id')
    //        ->where('s.user_id', auth()->id())
      //      ->whereRaw('si.id is null')
        //    ->orderByDesc('s.created_at')
          //  ->select('s.id')
          //  ->first();

     //     if (!is_null('active_card'))
      //    {
       //       return $active_card->id;
        //  } }

    public function orders()
    {
        return $this->hasMany('App\CertainOrder');
    }



    //Burası ShoppingCard Sınıfı yani Sepet sınıfı

    public function certainOrders()
    {
        return $this->hasMany('App\CertainOrder');
    }

    public function shopping_card_product_quantity()
    {
        return DB::table('shopping_card')->where('sepet_id' , $this->id)->sum('quantity');
    }

}
