<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class CertainOrder extends Model
{
    protected $table = "certain_orders";
    protected $guarded = [];


    public function shoppingCard()
    {
        return $this->belongsTo(ShoppingCard::class, 'sepet_id');
        //sepet_id is a foreign key.
    }


}
