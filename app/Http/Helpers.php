<?php


use Illuminate\Http\Request;

class Helpers
{

    public static function diffForHumans($date)
    {
       return \Carbon\Carbon::parse($date)->diffForHumans();
    }


    public  function logdump(Request $request)
    {

        $array = array(
            1 => $_COOKIE,
            2 => $request->session()->all(),
        );

        echo '<script>';
        echo 'console.log('. json_encode($array[1], JSON_HEX_TAG) .')';
        echo '</script>';

        echo '<script>';
        echo 'console.log('. json_encode($array[2], JSON_HEX_TAG) .')';
        echo '</script>';


    }

}
