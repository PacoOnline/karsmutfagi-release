<?php

namespace App\Http\Controllers;

use App\Category;
use App\Order;
use App\CertainOrder;
use App\ShoppingCardProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Http\Controllers\VoyagerAuthController;

class OrderController extends Controller
{
   public function index()
   {
       if (!auth()->check())
       {
           return redirect()->route('kullanici.giris-yap');
       }
       $orders =  \DB::table('certain_orders')
           ->orderBy('id', 'desc')
           ->where('user_id',  auth()->id())
           //->limit('1')
           ->get();
       return view('orders', compact('orders'));
   }

    public function wholeorder()
    {
        if(!Auth::user()->can('browse_admin'))
        {
            return redirect()->back();
        }
        $wholeorders =  \DB::table('certain_orders')
            ->orderBy('id', 'desc')
            //->where('user_id',  auth()->id())
            //->limit('1')
            ->get();
        return view('ordersforadmin', compact('wholeorders'));
    }

    //SiparisController sayfamiz

    public function orderdetail($id)
    {
        if (!auth()) {
            return redirect()->back();

        } else
        {
            $orderDetails = CertainOrder::with('ShoppingCard.certainOrders')
                ->where('certain_orders.id', $id)->firstorFail();
            $products = DB::table("shopping_card_product")
                ->join("products", "shopping_card_product.urun_id", "=", "products.id")
                ->where("sepet_id", '=', $orderDetails['sepet_id'])
                ->get();
            $getqty = DB::table("shopping_card_product")
                ->join("products", "shopping_card_product.urun_id", "=", "products.id")
                ->where("sepet_id", '=', $orderDetails['sepet_id'])
                ->get();
        }

        return view('orderdetails', compact('orderDetails', 'products'));
    }}

