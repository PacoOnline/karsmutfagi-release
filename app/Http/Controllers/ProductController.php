<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use Illuminate\Support\Facades\DB;


class ProductController extends Controller
{

    public function ProductList()
    {
        $products = \DB::table('products')
            ->orderBy('id', 'desc')
            //->where('featured',  1)
            ->limit('1')
            ->get();

        return view('home', compact('products'));
    }

    ####this is how i do list post by category names(id's)########
    #####should be developed more clean ##############

    public function ProductDetailFunction($id)
    {

      // $PostDetails = Post::find($id);
      $ProductDetails = Product::where('slug', $id)->first();

      return view('product-details-2', compact('ProductDetails'));  }

}


