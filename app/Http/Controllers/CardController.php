<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ShoppingCardProduct;
use App\ShoppingCard;
use App\Category;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;



class CardController extends Controller
{

    public function store (Request $request)
    {
        Cart::add($request->id, $request->short_description , $request->quantity, $request->price)
        ->associate('App\Product');
        return redirect()->route('sepet.index')->with('success', true);
    }


    public function paymentindex()
    {
        if (!auth()->check())
        {
           return redirect()->route('kullanici.giris-yap');
        }
        else if (count(Cart::content())==0)
        {
            $errors = ['Ödeme işlemine devam edebilmek için sepetinizde ürün bulunmalıdır'];
             return redirect()->route('sepet.index')->withErrors($errors);
        }

        return view('payment');
    }


    public function index()
    {
        return view('user.shoppingcart');
    }

    public function add(Request $request)
    {

            if ($request['quantity'] < 1)
                $request['quantity'] = 1;

           if(!auth()->check())
            {
                return redirect()->route('kullanici.giris-yap');
            }
            else
            {

                $urun = Product::find(request('id'));
                $cartItem =  Cart::add($urun->id, $urun->short_description, $request['quantity'], $urun->price, 1 , ['cover_photo'=>$urun->cover_photo ,
                'product_category'=>$urun->product_category ,'slug'=>$urun->slug]);
                $active_card_id = session('active_card_id');
                if (!isset($active_card_id))
                {
                    $active_card = ShoppingCard::create([
                        'user_id' => auth()->id(),
                    ]);
                    $active_card_id = $active_card->id;
                    session()->put('active_card_id', $active_card_id);
                }
                ShoppingCardProduct::updateOrCreate(
                    ['sepet_id' => $active_card_id, 'urun_id'=> $urun->id ],
                    ['quantity'=>$request['quantity'], 'price'=>$urun->price, 'status'=>'pending']
                );

                return redirect()->back()
                ->with('success', true);
            }
    }

    public function delete($rowid)
    {
        if (auth()->check())
        {
            $active_card_id = session('active_card_id');
            $cartItem = Cart::get($rowid);
            ShoppingCardProduct::where('sepet_id', 'active_card_id')->where('urun_id', $cartItem->id)->delete();
        }
        Cart::remove($rowid);
        return redirect()->route('sepet.index');

    }

    public function destroy()
    {
        if (auth()->check())
        {
            $active_card_id = session('active_card_id');
            ShoppingCardProduct::where('sepet_id', 'active_card_id')->delete();
        }
      Cart::destroy();
    return redirect()->route('sepet.index');

    }

    public function update($rowid)
    {
     Cart::update($rowid, request('quantity'));
     //   $getlastquantity = \DB::table('shopping_card')
      //      //burada join yazmam gerekiyor
       //     ->join("shopping_card_product", "shopping_card.id", "=", "shopping_card_product.sepet_id")
        //    ->orderBy('id', 'desc')
        //   ->where('user_id',  auth()->id())
        //   ->limit('1')
        //   ->update(['quantity' => \request('quantity')]);
         //buradaki çözüm muhtemelen active_card_id'den carda ulaşıp oradan o kartı update etmek.

           session()->flash('success', true);
           return(back());
    }

}
