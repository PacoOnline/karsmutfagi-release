<?php


namespace App\Http\Controllers;


use App\Mail\UserRegistrationMail;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class FrontEndUserController extends Controller
{
    public function viewlogin()
    {
        return view('user/sign-in');
    }

    public function signin()
    {
      $this->validate(request(),[
         'email'=>'required|email',
          'password'=>'required'
    ] );
      if (auth()->attempt(['email'=>request('email'),'password'=>request('password')], request()->has('remember_token')))
        {
             request()->session()->regenerate();
        //    return redirect()->back();
           return redirect()->intended();
        }
      else
      {
        $errors = ['email'=>'Hatalı giriş', 'password' =>'Şifreler uyuşmuyor'];
        return  redirect()->back()->withErrors($errors);
      }
    }

    public function viewsignup()
    {

        return view('user/sign-up');
    }

    public function sitemap()
    {
        return response()->file('/public/sitemap.xml');
    }

    public function signup()
    {

        $this->validate(request(),
        [
          'name'=> 'required|min:3| max:30',
          'surname'=> 'required|min:2| max:15',
          'email'=> 'required| email|unique:users',
          'phone_number'=> 'required|min:11|max:11|unique:users',
          'password'=>'required|confirmed|min:5| max:20',

        ]);

         $frontenduserlogincheck = User::create([
             'name'=> request('name'),
             'surname'=> request('surname'),
             'password'=> Hash::make(request('password')),
             'email'=> request('email'),
             'phone_number'=> request('phone_number'),
             'remember_token'=> Str::random(60),
             'role_id'=> 4,
             'is_active'=> 0
         ]);

        //Mail::to(request('email'))->send(new UserRegistrationMail($frontenduserlogincheck));

        $errors = ['email'=>'Bu Email adresi daha önce kullanılmış', 'password' =>'Şifreler Uyuşmuyor', 'phone_number'=>'Bu telefon numarası daha önce kullanılmış'];
        Auth()->login($frontenduserlogincheck);
        return redirect()->route('anasayfa');
    }


    public function myaccount()
    {
        if (!auth()->check())
        {
            return redirect()->route('kullanici.giris-yap');
        }

        return view('user.myaccount');
    }

    public function track()
    {

        return view('user.track');
    }

     public function logout()
     {
         \auth()->logout();
         request()->session()->flush();
         request()->session()->regenerate();
         return redirect()->route('anasayfa');
     }

}
