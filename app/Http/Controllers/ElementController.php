<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Product;
use App\Category;
use Illuminate\Support\Facades\DB;


class ElementController extends Controller
{

    public function ProductList()
    {
        $HomeAssociateImages = \DB::table('home_associate_images')
            ->orderBy('id', 'desc')
            //->where('featured',  1)
            ->get();
        $Home_Route_Products = DB::table("products")
           // ->join("products", "shopping_card_product.urun_id", "=", "products.id")
            ->where("id", '=' ,$HomeAssociateImages['id'])
            ->get();

        return view('home', compact('HomeAssociateImages','Home_Route_Products'));
    }

    public function logdump(Request $request)
    {

        $array = array(
          1 => $_COOKIE,
          2 => $request->session()->all(),
        );


        echo '<script>';
        echo 'console.log('. json_encode($array[1], JSON_HEX_TAG) .')';
        echo '</script>';

        echo '<script>';
        echo 'console.log('. json_encode($array[2], JSON_HEX_TAG) .')';
        echo '</script>';


    }
}
