<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use Illuminate\Support\Facades\DB;



class SearchController extends Controller
{

    public function search (Request $request)
    {
       $search = $request->get('search');
       $productsformysearch = \DB::table('products')
       ->orderBy('id', 'desc')
       ->where('short_description',  'like', '%' .$search. '%')
       ->orWhere('long_description',  'like', '%' .$search. '%')

           ->get();
       return view('searchresults', ['productsformysearch' => $productsformysearch ]);
    }

}


