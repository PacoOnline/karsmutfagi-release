<?php


namespace App\Http\Controllers;
use App\CertainOrder;
use App\Order;
use App\ShoppingCard;
use App\ShoppingCardProduct;
use Config;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Iyzipay\Model\Address;
use Iyzipay\Model\BasketItem;
use Iyzipay\Model\BasketItemType;
use Iyzipay\Model\Buyer;
use Iyzipay\Model\CheckoutFormInitialize;
use Iyzipay\Model\Currency;
use Iyzipay\Model\Locale;
use Iyzipay\Model\PaymentGroup;
use Iyzipay\Request\RetrieveCheckoutFormRequest;
use Iyzipay\Options;
use Iyzipay\Request;
use Iyzipay\Request\CreateCheckoutFormInitializeRequest;
use Gloudemans\Shoppingcart\Facades\Cart;
use League\CommonMark\Input;
use Illuminate\Http\Request as LaravelRequest;
use Prophecy\Argument\Token\CallbackToken;
use Ramsey\Uuid\Generator\RandomBytesGenerator;
use Symfony\Component\Console\Input as LaravelInput;
use Illuminate\Support\Str;

class PricingController extends Controller
{
         public function paytr(LaravelRequest $newRequest)
            {

            ## 1. ADIM için örnek kodlar ##

            ####################### DÜZENLEMESİ ZORUNLU ALANLAR #######################
            #
            ## API Entegrasyon Bilgileri - Mağaza paneline giriş yaparak BİLGİ sayfasından alabilirsiniz.
           $tuccarnumara =  $merchant_id 	= '195900';
           $tuccaranahtar =  $merchant_key 	= '3RwdKNpW5Nwme9CG';
           $tuccartuz =  $merchant_salt	= 'x4cWCGbFQ9FEhoUN';
            #
            ## Müşterinizin sitenizde kayıtlı veya form vasıtasıyla aldığınız eposta adresi
           $musterimail =  $email = $newRequest->Input('email');

            #price'ın 100 olması case'ini de hallet
           /* if (Cart::subtotal()>100)
            {
                $newprice = intval(Cart::subtotal());
            }
            elseif(Cart::subtotal()<100)
            {
                $newprice = Cart::subtotal()*100;
            }
            else
            {
                $newprice = Cart::subtotal()*100;
            }
          */

           $newprice = Cart::subtotal();

           $trimcomma = str_replace(',', '', $newprice);
           $trimdot = str_replace('.', '', $trimcomma);

           ## Tahsil edilecek tutar.
           $musteriodemefiyati = $payment_amount = $trimdot; //9.99 için 9.99 * 100 = 999 gönderilmelidir.
            #
            ## Sipariş numarası: Her işlemde benzersiz olmalıdır!! Bu bilgi bildirim sayfanıza yapılacak bildirimde geri gönderilir.
           $musterirandomkey =  $merchant_oid = Str::random(5);
            #
            ## Müşterinizin sitenizde kayıtlı veya form aracılığıyla aldığınız ad ve soyad bilgisi
           $musteriisim =  $user_name = $newRequest->Input('name');
            #
            ## Müşterinizin sitenizde kayıtlı veya form aracılığıyla aldığınız adres bilgisi
           $musteriadres =  $user_address = $newRequest->Input('address');
            #
            ## Müşterinizin sitenizde kayıtlı veya form aracılığıyla aldığınız telefon bilgisi
           $musteritelefon =  $user_phone = $newRequest->Input('phonenumber');
            #
            ## Başarılı ödeme sonrası müşterinizin yönlendirileceği sayfa
            ## !!! Bu sayfa siparişi onaylayacağınız sayfa değildir! Yalnızca müşterinizi bilgilendireceğiniz sayfadır!
            ## !!! Siparişi onaylayacağız sayfa "Bildirim URL" sayfasıdır (Bakınız: 2.ADIM Klasörü).
            $odemebasarili =  $merchant_ok_url = "https://www.development.karsmutfagi.com/odeme/basarili";
            #
            ## Ödeme sürecinde beklenmedik bir hata oluşması durumunda müşterinizin yönlendirileceği sayfa
            ## !!! Bu sayfa siparişi iptal edeceğiniz sayfa değildir! Yalnızca müşterinizi bilgilendireceğiniz sayfadır!
            ## !!! Siparişi iptal edeceğiniz sayfa "Bildirim URL" sayfasıdır (Bakınız: 2.ADIM Klasörü).
            $odemebasarisiz = $merchant_fail_url = "https://www.development.karsmutfagi.com/odeme/basarisiz ";
            #
            ## Kullanıcının IP adresi
            if( isset( $_SERVER["HTTP_CLIENT_IP"] ) ) {
                $ip = $_SERVER["HTTP_CLIENT_IP"];
            } elseif( isset( $_SERVER["HTTP_X_FORWARDED_FOR"] ) ) {
                $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
            } else {
                $ip = $_SERVER["REMOTE_ADDR"];
            }

            ## !!! Eğer bu örnek kodu sunucuda değil local makinanızda çalıştırıyorsanız
            ## buraya dış ip adresinizi (https://www.whatismyip.com/) yazmalısınız. Aksi halde geçersiz paytr_token hatası alırsınız.
            $musteriip = $user_ip = $ip;

            $hash = base64_encode( hash_hmac('sha256', $musterirandomkey.$tuccartuz.$musteriodemefiyati, $merchant_key, true) );


            $getlastcard = \DB::table('shopping_card')
                ->orderBy('id', 'desc')
                ->where('user_id',  auth()->id())
                ->limit('1')
                ->get();

            if(isset($getlastcard[0]->id))
            {
                $createorder = Order::create([
                'name'=> $newRequest->Input('name'),
                'city'=> $newRequest->Input('city'),
                'address'=> $newRequest->Input('address'),
                'email'=> $newRequest->Input('email'),
                'phonenumber'=> $newRequest->Input('phonenumber'),
                'notes'=> $newRequest->Input('notes'),
                'shopping_card_id'=> $getlastcard[0]->id,
                'sepet_id'=>$getlastcard[0]->id,
                'price'=> $musteriodemefiyati,
                'user_id'=> auth()->id(),
                'token'=> $musterirandomkey,
                'hash'=> $hash,
               'status'=> ('Sipariş Alınmıştır'),
                ]);
                $error = "";

            }
           $newRequest->session()->put('usersid' ,auth()->id());
            $session_data = session('active_card_id');
            Cart::destroy();
            session()->forget('active_card_id');

            $products = DB::table("shopping_card_product")
                    ->join("products", "shopping_card_product.urun_id", "=", "products.id")
                    ->where("sepet_id", '=' ,$getlastcard[0]->id)
                    ->get();


            foreach ($products as $item=>$key)
            {
                $key->short_description;

            }
            ## Müşterinin sepet/sipariş içeriği
            $musterisepeti =  $user_basket =

            $user_basket = base64_encode(json_encode(array(
                array("ŞARKÜTERİ ÜRÜNÜ", "Sipariş Tutarında belirtilmiştir", 1),
            )));

	        /* ÖRNEK $user_basket oluşturma - Ürün adedine göre array'leri çoğaltabilirsiniz
	        $user_basket = base64_encode(json_encode(array(
		    array("Örnek ürün 1", "18.00", 1), // 1. ürün (Ürün Ad - Birim Fiyat - Adet )
		    array("Örnek ürün 2", "33.25", 2), // 2. ürün (Ürün Ad - Birim Fiyat - Adet )
		    array("Örnek ürün 3", "45.42", 1)  // 3. ürün (Ürün Ad - Birim Fiyat - Adet )
	        )));
	        */
            #########################################################################
            return view('paytrpaymentform' , compact('odemebasarili','musteritelefon','odemebasarisiz','ip','email',
                'musterimail','musterisepeti', 'musteriadres','musteriip','musteriisim',
                'musteriodemefiyati','error','musterirandomkey','tuccartuz','tuccaranahtar','tuccarnumara','hash'));

           }

           public function PayTrFailure()
           {
               echo "ödeme başarısız oldu lütfen tekrar deneyiniz ya da bankanızla iletişime geçiniz.";
           }

         public function Success(LaravelRequest $easyrequest)
         {
             $getlastorder = DB::table('orders')
                 ->orderBy('id', 'desc')
                 ->where('user_id',  auth()->id())
                 ->limit('1')
                 ->get();

            $createorder = CertainOrder::create([
                 'name'=> $getlastorder[0]->name,
                 'city'=> $getlastorder[0]->city,
                 'address'=> $getlastorder[0]->address,
                 'email'=> $getlastorder[0]->email,
                 'phonenumber'=> $getlastorder[0]->phonenumber,
                 'notes'=> $getlastorder[0]->notes,
                 'sepet_id'=> $getlastorder[0]->shopping_card_id,
                 'price'=> Cart::subtotal(),
                 'user_id'=> auth()->id(),

             ]);

            return(\redirect('/siparis/'));
         }

        public function FonksOk()
        {
            echo "OK";
            exit;
        }

         public function Routeclear()
        {
        Artisan::call('route:clear');
        }

}
