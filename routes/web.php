<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\AuthController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
 //   return view('home');
//});

Route::get('/anasayfa', function () {
    return view('karsmutfagi.home');
});

Route::get('/iletisim', function () {
    return view('contact');
});

Route::get('/kategoriler', function () {
    return view('categories');
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::view('/payment-test','payment-test');


Route::get('/iptal-iade-kosullari', function () {
    return view('cancelllationandrefundconditions');
});
Route::get('/hakkimizda', function () {
    return view('aboutus');
});

Route::get('/gizlilik-politikası', function () {
    return view('privacypolicy');
});

Route::get('/mesafeli-satis-politikasi', function () {
    return view('distancesalescontract');
});

View::composer('*', function ($view) {

    $MaleProductLists = \DB::table('products')
    ->orderBy('id', 'Desc')
    ->where('product_category', '=', '7')
    ->limit('1')
    ->get();
     $view->with('MaleProductLists',$MaleProductLists);
});



View::composer('*', function ($view) {

    $FemaleProductLists = \DB::table('products')
    ->orderBy('id', 'Desc')
    ->where('product_category', '=', '6')
    ->limit('1')
    ->get();
     $view->with('FemaleProductLists',$FemaleProductLists);
});



View::composer('*', function ($view) {

    $AccessoryProductLists = \DB::table('products')
    ->orderBy('id', 'Desc')
    ->where('product_category', '=', '5')
    ->limit('1')
    ->get();
     $view->with('AccessoryProductLists',$AccessoryProductLists);
});



View::composer('*', function ($view) {

    $FeaturedHomeImages = \DB::table('featured_images')
    ->orderBy('id', 'Desc')
    //->where('product_category', '=', '5')
    ->limit('1')
    ->get();
     $view->with('FeaturedHomeImages',$FeaturedHomeImages);
});


View::composer('*', function ($view) {

    $home_associate_images = \DB::table('home_associate_images')
        ->orderBy('id', 'Desc')
        //->where('product_category', '=', '5')
        //->limit('1')
        ->get();
    $view->with('home_associate_images',$home_associate_images);
});

View::composer('*', function ($view) {

    $home_long_images = \DB::table('home_long_images')
        ->orderBy('id', 'Desc')
        //->where('product_category', '=', '5')
        //->limit('1')
        ->get();
    $view->with('home_long_images',$home_long_images);
});


View::composer('*', function ($view) {

    $FeaturedProductLists = \DB::table('products')
    ->orderBy('id', 'Desc')
    //->where('product_category', '=', '3')
    ->limit('10')
    ->get();
     $view->with('FeaturedProductLists',$FeaturedProductLists);
});

View::composer('*', function ($view) {

    $CategoryLists = \DB::table('categories')
    ->orderBy('id', 'Desc')
    //->where('product_category', '=', '3')
    //->limit('10')
    ->get();
    $view->with('CategoryLists',$CategoryLists);
});

View::composer('*', function ($view) {

    $SliderImages = \DB::table('sliders')
        ->orderBy('id', 'Desc')
        //->where('product_category', '=', '3')
        //->limit('10')
        ->get();
    $view->with('SliderImages',$SliderImages);
});

Route::post('/odemesonuc', 'PricingController@senttokendata');
Route::group(['middleware'=>'auth', 'prefix'=>'sepet'], function() {
    Route::get('/cart', 'CardController@add')->name('cart');
});
Route::group(['prefix' => 'sepet'], function()
{
Route::get('/', 'CardController@index')->name('sepet.index');
Route::post('/ekle', 'CardController@add')->name('sepet.ekle');
Route::delete('/kaldir/{rowid}', 'CardController@delete')->name('sepet.kaldir');
Route::delete('/bosalt', 'CardController@destroy')->name('sepet.bosalt');
Route::post('/guncelle/{rowid}', 'CardController@update')->name('sepet.guncelle');
Route::patch('/guncelle/{rowid}', 'CardController@update')->name('sepet.guncelle');

});

Route::get('/', function () {
    return Helpers::logdump();
});
Route::get('/sitemap', 'FrontEndUserController@sitemap');
Route::get('/admin/siparis', 'OrderController@wholeorder')->name('siparis.admin')->middleware('auth');
Route::get('/siparis', 'OrderController@index')->name('siparis.index');
Route::get('/siparis/SP-{id}', 'OrderController@orderdetail')->name('siparis.detay');

Route::get( "/" ,  'ViewController@viewhomepage' )->name('anasayfa');
Route::get("/kategori/{slug}" , 'CategoryController@CategoryDetails');
Route::get("/urun/{slug}" , 'ProductController@ProductDetailFunction');
Route::get('/search', 'SearchController@Search');
Route::get('/siparis/takip', 'FrontEndUserController@track');

Route::group(['prefix' => 'kullanici'], function () {
    Route::get('/giris-yap', 'FrontEndUserController@viewlogin')->name('kullanici.giris-yap');
    Route::post('/giris-yap', 'FrontEndUserController@signin')->name('kullanici.giris-yap');
    Route::get('/kaydol', 'FrontEndUserController@viewsignup')->name('kullanici.kaydol');
    Route::post('/kaydol', 'FrontEndUserController@signup')->name('kullanici.kaydol');
    Route::post('/oturumu-kapat', 'FrontEndUserController@logout')->name('kullanici.oturumukapat');
});

Route::group(['prefix' => 'odeme', 'middleware'=>'auth'], function () {
Route::get('/', 'CardController@paymentindex')->name('odeme.index');
Route::post('/odeme-yap', 'PricingController@paytr')->name('odeme.odemeyap');
});

Route::post('/odeme/basarili','PricingController@FonksOk');
Route::get('/odeme/basarili', 'PricingController@Success');

Route::get('login/facebook', 'LoginController@redirectToProvider');
Route::get('login/facebook/callback', 'LoginController@handleProviderCallback');

Route::get('/clearroute','PricingController@Routeclear');

Route::get('/kullanici/hesabim', 'FrontEndUserController@myaccount');
Route::get('/test/mail' , function()
{
    $kullanici = \App\User::find(1);
 return new App\Mail\UserRegistrationMail($kullanici);
}
);




