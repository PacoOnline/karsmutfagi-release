<link href="/img/fazifavicon2.ico" rel="shortcut icon"/>
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Şal Tokası</title>

                    <header class="shop_header_area carousel_menu_area">
                       <div class="carousel_top_header black_top_header row m0">
                         <div class="container">
                             <div class="carousel_top_h_inner">
                             <div class="float-md-left">
                                <div class="top_header_left">
                                    <div class="selector">
                                        <select class="language_drop" name="countries" id="countries" style="
                                        width:300px;">
                                          <option value='yt' data-image="/img/icon/flag-3.png" data-imagecss="flag yt" data-title="Turkish">Türkçe</option>
                                        </select>
                                    </div>
                                    <select class="selectpicker usd_select">
                                        <option>TRY</option>

                                    </select>
                                </div>
                            </div>
                            <div class="float-md-right">
                                <ul class="account_list">
                                    @guest
                                    <li><a href="{{ route('kullanici.giris-yap') }}">Giriş yap</a></li>
                                    <li><a href="{{ route('kullanici.kaydol') }}">Kayıt ol</a></li>
                                    @endguest
                                    @auth
                                    <li><a href="/kullanici/hesabim">Hesabım</a></li>
                                    <li><a href="#">İstek listesi</a></li>
                                    <li><a href="/siparis">Siparişlerim</a></li>
                                    <li><a href="#"  onclick="event.preventDefault(); document.getElementById('logout-form').submit()" >Çıkış Yap</a></li>
                                        <form id="logout-form" action="{{ route('kullanici.oturumukapat') }}" method="post" style="display: none">
                                        @csrf
                                        </form>
                                    @endauth
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="carousel_menu_inner">
                    <div class="container">
                        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                            <a class="navbar-brand" href="/"><img style="margin-left: 18pt" height="120px" width="150px" id="respoumut" src="/img/fazilogo.jpeg" alt=""></a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav ml-5 mr-auto">
                                    <li class="nav-item dropdown submenu active">
                                        <a class="nav-item" href="{{ route('anasayfa') }}" role="button">
                                        Anasayfa
                                        </a>
                                     </li>

                                    <li class="nav-item dropdown submenu">
                                        <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Ürünler <i class="fa fa-angle-down" aria-hidden="true"></i>
                                        </a>
                                        <ul class="dropdown-menu">
                                              @foreach($CategoryLists as $CategoryList)
                                        <li><a href="/kategori/{{$CategoryList->slug}}">{{$CategoryList->name}}</a></li>
                                        @endforeach
                                        </ul>
                                    </li>
                                    <li class="nav-item"><a class="nav-link" href="/nasil-kullanilir">Nasıl Kullanılır?</a></li>

                                    <li class="nav-item"><a class="nav-link" href="/iletisim">İLETİŞİM</a></li>
                                </ul>


                                <ul class="navbar-nav justify-content-end">
                                    <li class="user_icon"><a href="/kullanici/hesabim"><i class="icon-user icons"></i></a></li>
                                    <li class="cart_cart"><a href="/sepet"><i class="icon-handbag icons"></i></a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </header>
