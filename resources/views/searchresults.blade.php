<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179398468-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179398468-1');
        </script>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    @include('karsmutfagi.partials.favicon')

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Arama Sonuçları</title>

        <!-- Icon css link -->
        <link href="/css/font-awesome.min.css" rel="stylesheet">
        <link href="/vendors\line-icon\css\simple-line-icons.css" rel="stylesheet">
        <link href="/vendors/elegant-icon/style.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="/css/bootstrap.min.css" rel="stylesheet">

        <!-- Rev slider css -->
        <link href="/vendors/revolution/css/settings.css" rel="stylesheet">
        <link href="/vendors/revolution/css/layers.css" rel="stylesheet">
        <link href="/vendors/revolution/css/navigation.css" rel="stylesheet">

        <!-- Extra plugin css -->
        <link href="/vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
        <link href="/vendors/bootstrap-selector/css/bootstrap-select.min.css" rel="stylesheet">
        <link href="/vendors/jquery-ui/jquery-ui.css" rel="stylesheet">

        <link href="/css/style.css" rel="stylesheet">
        <link href="/css/responsive.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="/karsmutfagi-style/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="/karsmutfagi-style/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="/karsmutfagi-style/css/themify-icons.css" type="text/css">
        <link rel="stylesheet" href="/karsmutfagi-style/css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="/karsmutfagi-style/css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="/karsmutfagi-style/css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="/karsmutfagi-style/css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="/karsmutfagi-style/css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="/karsmutfagi-style/css/style.css" type="text/css">

    </head>
    <style>
        .categories_banner_area{
            background:  url("/karsmutfagi-style/img/cheese.jpg") no-repeat scroll center center;
            background-size: cover;
            position: relative;
            z-index: 3;
        }
    </style>

    <body>

        <!--================Menu Area =================-->
      @include('karsmutfagi.partials.header')
        <!--================End Menu Area =================-->

        <!--================Categories Banner Area =================-->
        <section class="categories_banner_area">
            <div class="container">
                <div class="c_banner_inner">
                    <h3>Arama sonuçları</h3>
                    <ul>
                        <li><a href="#">Anasayfa</a></li>
                        <li><a href="#">Kategoriler</a></li>
                        <li class="current"><a href="#">Arama sonuçları</a></li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="categories_product_main p_80">
            <div class="container">
                <div class="categories_main_inner">
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="showing_fillter">
                                <div class="row m0">
                                </div>
                            </div>
                            <div class="categories_product_area">
                                <div class="row">
                            @if(isset($productsformysearch))
                           @foreach($productsformysearch as $productsforsearch)
                                    <div class="col-lg-4 col-sm-6">
                                        <div class="l_product_item">
                                            <div class="l_p_img">
                                                <img href="/urun/{{$productsforsearch->slug}}" width=270px height=320px src="/storage/{{$productsforsearch->cover_photo}}" alt="">
                                            </div>
                                            <div class="l_p_text">
                                               <ul>
                                                    <li><a class="add_cart_btn" href="/urun/{{$productsforsearch->slug}}">Ürüne git</a></li>
                                                </ul>
                                                <h4><a>{{$productsforsearch->short_description}}</a></h4>
                                                <h5>{{$productsforsearch->price}}TL</h5>
                                            </div>
                                        </div>
                                    </div>
                           @endforeach
                           @endif
                            </div>

                            </div>
                         </div>
                        <div class="col-lg-3">
                            <div class="categories_sidebar">
                                <aside class="l_widgest l_p_categories_widget">
                                    <div class="l_w_title">
                                        <h3>Kategoriler</h3>
                                    </div>
                                    @foreach($CategoryLists as $CategoryList)
                                    <ul class="navbar-nav">
                                        <li style="background-color: white;" class="nav-item">
                                            <a style="background-color: white;" class="nav-link" href="/kategori/{{$CategoryList->slug}}">{{$CategoryList->name}}
                                                <i class="icon_plus" aria-hidden="true"></i>
                                            <i class="icon_minus-06" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                    @endforeach

                                    </ul>
                                </aside>
                                <aside class="l_widgest l_menufacture_widget">
                                    <div class="l_w_title">
                                        <h3>Markalar</h3>
                                    </div>
                                    <ul>
                                        <li><a href="#">Kars Mutfağı</a></li>

                                    </ul>
                                </aside>
                                <aside class="l_widgest l_feature_widget">
                                    <div class="l_w_title">
                                        <h3>Öne çıkan ürünler</h3>
                                    </div>
                                    @foreach($FeaturedProductLists as $FeaturedProductList)
                                    <div class="media">
                                        <div class="d-flex">
                                            <img height="100px" width="100px" src="/storage/{{$FeaturedProductList->cover_photo}}" alt="">
                                        </div>
                                        <div class="media-body">
                                            <h4>{{$FeaturedProductList->short_description}}</h4>
                                            <h5>{{$FeaturedProductList->price}}TL</h5>
                                        </div>
                                    </div>
                                    @endforeach

                                    </div>
                                </aside>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================End Categories Product Area =================-->

        @include('karsmutfagi.partials.footer')

        <!---------  Navbar required assets --------------->
        <script src="/karsmutfagi-style/js/jquery-3.3.1.min.js"></script>
        <script src="/karsmutfagi-style/js/jquery.slicknav.js"></script>
        <script src="/karsmutfagi-style/js/main.js"></script>
        <!------------------------->



        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery-3.2.1.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <!-- Rev slider js -->
        <script src="vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <!-- Extra plugin css -->
        <script src="vendors/counterup/jquery.waypoints.min.js"></script>
        <script src="vendors/counterup/jquery.counterup.min.js"></script>
        <script src="vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="vendors/bootstrap-selector/js/bootstrap-select.min.js"></script>
        <script src="vendors/image-dropdown/jquery.dd.min.js"></script>
        <script src="js/smoothscroll.js"></script>
        <script src="vendors/isotope/imagesloaded.pkgd.min.js"></script>
        <script src="vendors/isotope/isotope.pkgd.min.js"></script>
        <script src="vendors/magnify-popup/jquery.magnific-popup.min.js"></script>
        <script src="vendors/vertical-slider/js/jQuery.verticalCarousel.js"></script>
        <script src="vendors/jquery-ui/jquery-ui.js"></script>

        <script src="js/theme.js"></script>
    </body>
</html>
