<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Kars Mutfağı">
    <meta name="keywords" content="Kaşar,Bal,Peynir,Kars">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kars Mutfağı</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    @include('karsmutfagi.partials.favicon')
    <!-- Css Styles -->
    <link rel="stylesheet" href="karsmutfagi-style/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="karsmutfagi-style/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="karsmutfagi-style/css/themify-icons.css" type="text/css">
    <link rel="stylesheet" href="karsmutfagi-style/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="karsmutfagi-style/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="karsmutfagi-style/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="karsmutfagi-style/css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="karsmutfagi-style/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="karsmutfagi-style/css/style.css" type="text/css">
    <style>
        .img{

        }
    </style>
</head>
<style>
    @media (min-width: 500px) {
        #slidermobiler {
            margin-left: 30%;

        }
    }
</style>

<body>


<!-- Header Section Begin -->
@include('karsmutfagi.partials.header')
<!-- Header End -->
<!-- Hero Section Begin -->
<section class="hero-section">
    <div class="hero-items owl-carousel">
        <div  class="single-hero-items set-bg" data-setbg="/karsmutfagi-style/img/optimized/banner_image.jpg">
            <div class="container">
                <div class="row">
                    <div id="slidermobiler" class="col-lg-7">
                        <h1><a style="color: white" >Doğal Peynirlerimiz</a></h1>
                        <p><a style="color: white">Ürünlerimizin tamamı buz aküleri içinde özel kutularda gönderilmektedir</a></p>

                    </div>
                </div>
            </div>
        </div>
        <!---->
        <div class="single-hero-items set-bg" data-setbg="/karsmutfagi-style/img/optimized/banner_image.jpg">
            <div class="container">
                <div class="row">
                    <div id="slidermobiler" class="col-lg-7">
                        <h1><a style="color: white" >Kargo aşamasında bozulmaz!</a></h1>
                        <p><a style="color: white">Ürünlerimizin tamamı buz aküleri içinde özel kutularda gönderilmektedir</a></p>

                    </div>
                </div>
            </div>
        </div>
        <!---->
        <!----->
        <div class="single-hero-items set-bg" data-setbg="/karsmutfagi-style/img/optimized/banner.jpg">
            <div class="container">
                <div class="row">
                    <div id="slidermobiler" class="col-lg-7">
                        <h1><a style="color: black" >Kargo aşamasında bozulmaz!</a></h1>
                        <p><a style="color: black">Ürünlerimizin tamamı buz aküleri içinde özel kutularda gönderilmektedir</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Hero Section End -->
<!-- Banner Section Begin -->
<div class="banner-section spad">
    <div class="container-fluid">
        <div class="row">
           @foreach($home_associate_images as $home_associate_image)
            <div class="col-lg-4">
                <div class="single-banner">
                    <img height="280px" width="570px" src="storage/{{$home_associate_image->image}}">
                    <div class="inner-text">
                        <h4><a style="color: white;" href="urun/{{$home_associate_image->product_slug}}">{{$home_associate_image->title}}</a></h4>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
<!-- Banner Section End -->
<!-- Deal Of The Week Section Begin-->
@foreach($home_long_images as $home_long_image)
<section class="deal-of-week set-bg spad" data-setbg="storage/{{$home_long_image->image}}">
    <div class="container">
        <div class="col-lg-6 text-center">
            <div class="section-title">
                <h2>{{$home_long_image->title}}</h2>
                    <p>{{$home_long_image->description}}</p>
                <div class="product-price">
                    @if(isset($home_long_image->del_price))
                   {{$home_long_image->del_price}}₺<br><span> Yerine sadece</span><br>{{$home_long_image->price}}₺
                    @else
                        {{$home_long_image->price}}₺
                    @endif
                </div>
            </div>
            <a href="/urun/{{$home_long_image->product_slug}}" class="primary-btn">Satın al</a>
        </div>
    </div>
</section>
@endforeach
<!-- Deal Of The Week Section End -->

<!-- Man Banner Section Begin -->
<section class="man-banner spad">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8">
                <div class="product-slider owl-carousel">
                 @foreach($FeaturedProductLists as $FeaturedProductList)
                    <div class="product-item">
                        <div class="pi-pic">
                            <img height="270px" width="330px" src="storage/{{$FeaturedProductList->cover_photo}}" alt="/urunler/{{$FeaturedProductList->slug}}">
                            <ul>
                                <li class="quick-view"><a href="/urun/{{$FeaturedProductList->slug}}">Ürünü incele</a></li>
                            </ul>
                        </div>
                        <div class="pi-text">
                            <a href="/urun/{{$FeaturedProductList->slug}}">
                                <h5>{{$FeaturedProductList->short_description}}</h5>
                            </a>
                            <div class="product-price">
                                {{$FeaturedProductList->price}}₺
                                <span>{{$FeaturedProductList->price+$FeaturedProductList->price*20/100}}₺</span>
                            </div>
                        </div>
                    </div>
                   @endforeach
                </div>
            </div>
            <div class="col-lg-3 offset-lg-1">
                <div  class="product-large set-bg m-large" data-setbg="/karsmutfagi-style/img/optimized/gravyer1-release.png">
                    <h3 style="color: white;">Kars Gravyer Peyniri</h3>
                    <a href="/urun/kars-bogatepe-gravyer-peyniri-500gr">Şimdi İncele!</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Man Banner Section End -->
@include('karsmutfagi.partials.footer')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.js"></script>

<script>
    $(document).ready(function){
        $("img").lazyload();
    }
</script>
<!-- Js Plugins -->
<script src="/karsmutfagi-style/js/jquery-3.3.1.min.js"></script>
<script src="/karsmutfagi-style/js/bootstrap.min.js"></script>
<script src="/karsmutfagi-style/js/jquery-ui.min.js"></script>
<script src="/karsmutfagi-style/js/jquery.countdown.min.js"></script>
<script src="/karsmutfagi-style/js/jquery.nice-select.min.js"></script>
<script src="/karsmutfagi-style/js/jquery.zoom.min.js"></script>
<script src="/karsmutfagi-style/js/jquery.dd.min.js"></script>
<script src="/karsmutfagi-style/js/jquery.slicknav.js"></script>
<script src="/karsmutfagi-style/js/owl.carousel.min.js"></script>
<script src="/karsmutfagi-style/js/main.js"></script>
<script src="/karsmutfagi-style/js/app.js"></script>


</body>

</html>
