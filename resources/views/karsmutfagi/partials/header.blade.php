<meta name="csrf-token" content="{{ csrf_token() }}">

<header class="header-section">
    <div class="header-top">
        <div class="container">
            <div class="ht-left">
                <div class="mail-service">
                    <i class=" fa fa-envelope"></i>
                    info@karsmutfagi.com
                </div>
                <div class="phone-service">
                    <i class=" fa fa-phone"></i>
                    +90 543 644 12 13
                </div>
            </div>
            <div class="ht-right">
                @guest
                <a href="{{ route('kullanici.kaydol') }}" class="login-panel">Kayıt Ol&nbsp;</a>
                <a href="{{ route('kullanici.giris-yap') }}" class="login-panel">Giriş Yap &nbsp;&nbsp;</a>
                @endguest
                 @auth
                        <a href="#" class="login-panel" onclick="event.preventDefault(); document.getElementById('logout-form').submit()" >Çıkış Yap</a>
                        <form id="logout-form" action="{{ route('kullanici.oturumukapat') }}" method="post" style="display: none">
                            @csrf
                        </form>
                    <a href="/kullanici/hesabim"  style="padding-right: 6px;" class="login-panel">Hesabım &nbsp;</a>
                    <a href="/siparis" style="padding-right: 2px;" class="login-panel">Siparişlerim &nbsp;</a>
                    @endauth

            </div>
        </div>
    </div>
    <div class="container">
        <div class="inner-header">
            <div class="row">
                <div class="col-lg-2 col-md-2">
                    <div class="logo">
                        <a href="/anasayfa">
                            <img height="75px" style="margin-top: -25px;"  src="/karsmutfagi-style/img/karsmutfagilogomuz.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-7 col-md-7">
                    <div class="advanced-search">
                        <form  action='/search' method="GET">
                        <button type="button" class="category-btn">Tüm Ürünler&nbsp;&nbsp;</button>
                        <div class="input-group">
                            <input type="search" style="margin-top: 12px;" name="search" placeholder="Aradığınız Ürünü Giriniz">
                            <button type="submit"><i style="padding: 15px;" class="ti-search"></i></button>
                        </div>
                       </form>
                    </div>
                </div>
                <div class="col-lg-3 text-right col-md-3">
                    <ul class="nav-right">
                        <li class="cart-icon">
                            <a  style="font-size: medium;" href="/sepet">
                                Sepetim
                                <img  href="/sepet" src="/karsmutfagi-style/img/shopping-cart.png">
                            </a>
                        </li>

                        @if(\Gloudemans\Shoppingcart\Facades\Cart::subtotal() ==0)

                        @else
                            <li style="font-size: small;">{{\Gloudemans\Shoppingcart\Facades\Cart::subtotal()}}₺</li>
                        @endif

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="nav-item">
        <div class="container">
            <div class="nav-depart">
                <div class="depart-btn">
                    <i class="ti-menu"></i>
                    <span>Kategoriler</span>
                    <ul class="depart-hover">
                        @foreach($CategoryLists as $CategoryList)
                            <li><a href="/kategori/{{$CategoryList->slug}}">{{$CategoryList->name}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <nav class="nav-menu mobile-menu">
                <ul>
                    <li class="active"><a href="/">Anasayfa</a></li>
                    <li><a href="/kategori/peynir">Peynir</a></li>
                    <li><a href="/kategori/tereyag">Tereyağı</a></li>
                    <li><a href="/kategori/bal">Bal</a></li>
                    <li><a href="/iletisim">İLETİŞİM</a></li>
                </ul>
            </nav>
            <div id="mobile-menu-wrap"></div>
        </div>
    </div>
</header>
