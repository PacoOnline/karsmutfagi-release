
<!-- Footer Section Begin -->
<style>
    .float{
        position:fixed;
        width:60px;
        height:60px;
        bottom:40px;
        right:40px;
        background-color:#25d366;
        color:#FFF;
        border-radius:50px;
        text-align:center;
        font-size:30px;
        box-shadow: 2px 2px 3px #999;
        z-index:100;
    }

    .my-float{
        margin-top:16px;
    }
</style>
<footer class="footer-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="footer-left">
                    <div class="footer-logo">
                        <a href="#"><img src="/karsmutfagi-style/karsmutfagi-black-border.png" alt=""></a>
                    </div>
                    <ul>
                        <li>İbrahim ağa Sok. No. 10  Merkez/Kars</li>
                         <br>
                        <li>İletişim No: <a href="tel:+905436441213">+90 543 644 12 13</a></li>
                        <li>Email: <a href="mailto:info@karsmutfagi.com">info@karsmutfagi.com</a></li>
                        <li style="color: white"><span>Havale ile ödeme yapmak için
                         <br>   AYHAN GEZİCİ - HALKBANK
                                <br>TR760001200948000001031911</span></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2 offset-lg-1">
                <div class="footer-widget">
                    <h5>Bilgi</h5>
                    <ul>
                        <li><a href="/hakkimizda">Hakkımızda</a></li>
                        <li><a href="/mesafeli-satis-politikasi">Mesafeli Satış Sözleşmesi</a></li>
                        <li><a href="/iptal-iade-kosullari">İade ve İptal Koşulları</a></li>
                        <li><a href="/gizlilik-politikası">Gizlilik Politikası</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="footer-widget">
                    <h5>Hesabım</h5>
                    <ul>
                        <li><a href="/kullanici/hesabim">Hesabım</a></li>
                        <li><a href="/siparis">Siparişlerim</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="newslatter-item">
                    <h5>Bizden Haberdar ol!</h5>
                    <p>Email bültenimize hemen abone olun, İndirimlerden ilk sen yararlanın!</p>
                    <form style="color: green" action="#" class="subscribe-form">
                        <input type="text" placeholder="Email Adresinizi Giriniz">
                        <button type="button" onClick="window.location.reload()">Abone Ol</button>
                    </form>
                   <img style="margin-top: 10px; margin-bottom: 5px;" height="80px" src="/karsmutfagi-style/img/Trpay-transparent.png">
                    <h6 style="color: white;">Ödemelerimizi PAYTR - Visa Güvencesi altında AutoSSL Güvenlik sertifikasıyla alıyoruz!</h6>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright-reserved">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copyright-text">
                     <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                     Copyright &copy;<script>document.write(new Date().getFullYear());</script> Tüm hakları Saklıdır <i class="fa fa-heart-o" aria-hidden="true"></i> <a href="tel:05386293141" style="font-size: medium;" target="_blank">Umut Ozan Özyıldırım</a>
                     <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </div>

                </div>
            </div>
        </div>
    </div>
</footer>


<!-- Footer Section End -->
