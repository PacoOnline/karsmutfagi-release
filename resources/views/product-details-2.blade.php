<!DOCTYPE html>
<html lang="zxx">
<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179398468-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-179398468-1');
    </script>

    <title>{{$ProductDetails->short_description}}</title>
	<meta charset="UTF-8">
	<meta name="description" content=" Divisima | eCommerce Template">
	<meta name="keywords" content="şal tokası, eCommerce, creative, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->
@include('karsmutfagi.partials.favicon')


    <!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,400i,700,700i" rel="stylesheet">


	<!-- Stylesheets -->
	<link rel="stylesheet" href="/divisima/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="/divisima/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="/divisima/css/flaticon.css"/>
	<link rel="stylesheet" href="/divisima/css/slicknav.min.css"/>
	<link rel="stylesheet" href="/divisima/css/jquery-ui.min.css"/>
	<link rel="stylesheet" href="/divisima/css/owl.carousel.min.css"/>
	<link rel="stylesheet" href="/divisima/css/animate.css"/>
	<link rel="stylesheet" href="/divisima/css/style.css"/>
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/vendors/line-icon/css/simple-line-icons.css" rel="stylesheet">
    <link href="/vendors/elegant-icon/style.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Rev slider css -->
    <link href="/vendors/revolution/css/settings.css" rel="stylesheet">
    <link href="/vendors/revolution/css/layers.css" rel="stylesheet">
    <link href="/vendors/revolution/css/navigation.css" rel="stylesheet">

    <!-- Extra plugin css -->
    <link href="/vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="/vendors/bootstrap-selector/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="/vendors/vertical-slider/css/jQuery.verticalCarousel.css" rel="stylesheet">

    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/responsive.css" rel="stylesheet">


	    <!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="/karsmutfagi-style/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/themify-icons.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/style.css" type="text/css">


</head>
<body>
<?php

$SimilarProducts = \DB::table('products')
->orderBy('id', 'desc')
//->where('featured',  1)
->where('product_category', '=',  $ProductDetails->category_id)
//->limit('1')
->get();

$ArrayOfImages  = $ProductDetails->images;
//$ArrayOfImages = explode(",", $ProductDetails->images);
//echo $ArrayOfImages[1];


$ArrayOfImages = json_decode($ProductDetails->images);
//print_r($ArrayOfImages);

?>
	@include('karsmutfagi.partials.header')

	<!-- Page info -->
	<div class="page-top-info">
		<div class="container">
			<h4>Ürün Detay</h4>
			<div class="site-pagination">
				<a href="">Anasayfa</a> /
				<a href="">Ürün</a> /
				<a href="">Ürün Detay</a>
			</div>
		</div>
	</div>
	<!-- Page info end -->
@if (session()->has('success'))
    <section class="shopping_cart_area mt-5">

        <div class="container">
            <div class="col-lg-12">
                <div class="alert alert-success" role="alert">
                    Ürününüz Sepete Başarıyla Eklendi
                </div>
            </div>
        </div>
    </section>
@endif

	<!-- product section -->
	<section class="product-section">
		<div class="container">
			<div class="back-link">
				<a href="{{route('anasayfa')}}"> &lt;&lt; Anasayfa'ya geri dön</a>
			</div>
			<div class="row">
				<div class="col-lg-6">
					<div class="product-pic-zoom">
						<img height="500px" width="600px" class="product-big-img" src="/storage/{{$ProductDetails->cover_photo}}" alt="">
					</div>
					<div class="product-thumbs" tabindex="1" style="overflow: hidden; outline: none;">
						<div class="product-thumbs-track">
							<div class="pt active"  data-imgbigurl="/storage/{{$ProductDetails->cover_photo}}"><img width='116px' height='116px' src="/storage/{{$ProductDetails->cover_photo}}" alt=""></div>
							@if(isset($ArrayOfImages))
                            @foreach ($ArrayOfImages as $ArrayOfImage)
							<div class="pt" width='116px' height='116px' data-imgbigurl="/storage/{{$ArrayOfImage}}">  <img src="/storage/{{$ArrayOfImage}}" alt=""></div>
							@endforeach
                            @endif
						</div>
					</div>
				</div>
				<div class="col-lg-6 product-details">
					<h2 class="p-title">{{$ProductDetails->short_description}}</h2>
					<h3 class="p-price">{{$ProductDetails->price}}TL</h3>
                    <h4 class="p-stock">Stokta: <span> {{$ProductDetails->stock_number}}ADET</span></h4>
					<div class="p-rating">
						<i class="fa fa-star-o"></i>
						<i class="fa fa-star-o"></i>
						<i class="fa fa-star-o"></i>
						<i class="fa fa-star-o"></i>
						<i class="fa fa-star-o fa-fade"></i>
					</div>
					<div class="p-review">
						<a></a>|<a >Yorum ekle</a>
					</div>
                    <div class="quantity">
                        <p>Ürün kodu</p>
                        <div><h5>{{ $ProductDetails->product_code }}</h5></div>
                    </div>
                    <form action="{{ route('sepet.ekle') }}" method="POST" >
                    <div class="quantity">
                    <p>Adet</p>
                        <div class="pro-qty">
                            <input id="quantity" name="quantity" min="1" value="1" title="Qty" max="{{ $ProductDetails->stock_number }}" >
                        </div>
                    </div>
            <!--    <input name="quantity" id="quantity" value="1" title="Qty" max="{{ $ProductDetails->stock_number }}" >       -->
            <!--    <input type="number" id="quantity" name="quantity" value="" min="1" max="{{$ProductDetails->stock_number}}"> -->
            <!--     <div class="pro-qty"><input type="number" id="quantity" name="quantity" min="1" value="" ></div>            -->
					<input type="hidden"  name="id" value="{{ $ProductDetails->id }}">
					<input type="hidden"  name="short_description" value="{{ $ProductDetails->short_description }}">
					<input type="hidden" name="price" value="{{ $ProductDetails->price}}">
                    @csrf
                    <input type="hidden" name="id" value="{{ $ProductDetails->id }}">
					<input type="submit"  value="Sepete Ekle" class="site-btn">
					</form>
					<div id="accordion" class="accordion-area">
						<div class="panel">
							<div class="panel-header" id="headingOne">
								<button class="panel-link active" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">Ürün Hakkında</button>
							</div>
							<div id="collapse1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
								<div class="panel-body">
									<p>{{$ProductDetails->long_description}}</p>
								</div>
							</div>
						</div>

						<div class="panel">
							<div class="panel-header" id="headingThree">
								<button class="panel-link" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">Kargo & İade</button>
							</div>
							<div id="collapse3" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
								<div class="panel-body">
									<h4>14 Gün içinde iade edilebilir</h4>
                                    <p>Ürünümüz size ulaştıktan sonra  <span>TÜKETİCİ HAKLARI MAHKEMESİNCE ALINAN KARARA GÖRE</span><br>Kullanılmamış olan ürünleri 14 gün içinde iade edebilirsiniz</p>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</section>
	<!-- product section end -->


	<!-- RELATED PRODUCTS section -->
	<section class="related-product-section">
		<div class="container">
			<div class="section-title">
				<h2>BENZER ÜRÜNLER</h2>
			</div>
			<div class="product-slider owl-carousel">
			@foreach($FeaturedProductLists as $FeaturedProductList)
				<div class="product-item">
					<div class="pi-pic">
						<img style="height:300px" src="/storage/{{$FeaturedProductList->cover_photo}}" alt="">
						<div class="pi-links">
							<a href="/urun/{{$FeaturedProductList->slug}}" class="add-card"><i class="flaticon-bag"></i><span>Ürüne git</span></a>
							<a  class="wishlist-btn"><i class="flaticon-heart"></i></a>
						</div>
					</div>
					<div class="pi-text">
						<h6>{{$FeaturedProductList->price}}TL</h6>
						<p>{{$FeaturedProductList->short_description}}</p>
					</div>
				</div>
            @endforeach

				</div>
			</div>
		</div>
	</section>
	<!-- RELATED PRODUCTS section end -->


	<!-- Footer section -->
     @include('karsmutfagi.partials.footer')
	<!-- Footer section end -->

    <!---------  Navbar required assets --------------->
    <script src="/karsmutfagi-style/js/jquery-3.3.1.min.js"></script>
    <script src="/karsmutfagi-style/js/jquery.slicknav.js"></script>
    <script src="/karsmutfagi-style/js/main.js"></script>
    <!--====== Javascripts & Jquery ======-->
	<script src="/divisima/js/jquery-3.2.1.min.js"></script>
	<script src="/divisima/js/bootstrap.min.js"></script>
	<script src="/divisima/js/jquery.slicknav.min.js"></script>
	<script src="/divisima/js/owl.carousel.min.js"></script>
	<script src="/divisima/js/jquery.nicescroll.min.js"></script>
	<script src="/divisima/js/jquery.zoom.min.js"></script>
	<script src="/divisima/js/jquery-ui.min.js"></script>
	<script src="/divisima/js/main.js"></script>
    <script src="/vendors/image-dropdown/jquery.dd.min.js"></script>
    <script src="/js/smoothscroll.js"></script>
    <script src="/vendors/isotope/imagesloaded.pkgd.min.js"></script>
    <script src="/vendors/isotope/isotope.pkgd.min.js"></script>
    <script src="/vendors/magnify-popup/jquery.magnific-popup.min.js"></script>
    <script src="/vendors/bootstrap-selector/js/bootstrap-select.min.js"></script>
    <script src="/js/theme.js"></script>
    </body>
</html>
