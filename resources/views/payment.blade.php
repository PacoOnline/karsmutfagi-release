<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

@include('karsmutfagi.partials.favicon')

<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Kars Mutfağı - Güvenli Ödeme</title>

    <!-- Icon css link -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="vendors/line-icon/css/simple-line-icons.css" rel="stylesheet">
    <link href="vendors/elegant-icon/style.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Rev slider css -->
    <link href="vendors/revolution/css/settings.css" rel="stylesheet">
    <link href="vendors/revolution/css/layers.css" rel="stylesheet">
    <link href="vendors/revolution/css/navigation.css" rel="stylesheet">

    <!-- Extra plugin css -->
    <link href="vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="vendors/bootstrap-selector/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="vendors/jquery-ui/jquery-ui.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <!-- Css Styles -->
    <link rel="stylesheet" href="/karsmutfagi-style/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/themify-icons.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/style.css" type="text/css">

</head>
<body>
<?php
header('Set-Cookie: ' . session_name() . '=' . session_id() . '; SameSite=None; Secure');

?>


<!--================Menu Area =================-->
@include('karsmutfagi.partials.header')
<!--================End Menu Area =================-->

<!--================Categories Banner Area =================-->
<section class="solid_banner_area">
    <div class="container">
        <div class="solid_banner_inner">
            <h3>Ödeme</h3>
            <ul>
                <li><a href="{{route('anasayfa')}}">Anasayfa</a></li>
                <li><a href="{{route('odeme.index')}}">Ödeme</a></li>
            </ul>
        </div>
    </div>
</section>
<?php
function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}
$ip = get_client_ip();
?>
<!--================End Categories Banner Area =================-->
<!--================Register Area =================-->
<section class="register_area p_100">
    <div class="container">
        <div class="register_inner">
            <div class="row">
                <div class="col-lg-7">
                    <div class="billing_details">
                        <h2 class="reg_title">Fatura Detayları</h2>
                        <form method="POST" action="{{ route('odeme.odemeyap') }}"  class="billing_inner row">
                            @csrf

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="name">İsim-Soyisim <span>*</span></label>
                                    <input type="text" class="form-control" name="name" id="name" aria-describedby="name" placeholder="" required>
                                </div>
                            </div>
                            <!----
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="last">Soyisim<span>*</span></label>
                                    <input type="text" class="form-control" id="surname" name="surname" aria-describedby="last" value="" required>
                                </div>
                            </div>
                           ---->
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="ctown">Şehir<span>*</span></label>
                                        <input type="text"  name="city" class="form-control" id="city" aria-describedby="city" required>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="address">Adres <span>*</span></label>
                                    <input type="text" name="address" class="form-control" id="address" aria-describedby="address" required>
                                </div>
                            </div>
                            <div class="hidden">
                                <input type="hidden" name="ip_address" id="ip_address" value="<?php echo ($ip); ?>">
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="email">Email Adresiniz <span>*</span></label>
                                    <input type="email" name="email" class="form-control" id="email" aria-describedby="email" required>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="phone">Telefon Numaranız <span>*</span></label>
                                    <input type="text" name="phonenumber" class="form-control" id="phonenumber" aria-describedby="phone" required>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="order">Sipariş Notları</label>
                                    <textarea name="notes" class="form-control" placeholder="Eğer var ise siparişinize eklemek istediğiniz notlar" id="notes" rows="3"></textarea>
                                </div>
                            </div>
                             </div>
                                        </div>
                <div class="col-lg-5">
                    <div class="order_box_price">
                        <h2 class="reg_title">Siparişiniz</h2>
                        <div class="payment_list">
                            <div class="price_single_cost">
                                @foreach(Cart::content() as $urunCartItem)
                                <h5>{{$urunCartItem->name}} <span>{{$urunCartItem->price}}₺</span></h5>
                                @endforeach
                                <h4>Ara Toplam <span>{{Cart::subtotal()}}₺</span></h4>
                                <h3><span class="normal_text">Toplam</span> <span>{{Cart::subtotal()}}₺</span></h3>
                            </div>
                            </div>
                        </div>
                        <button style="background-color: grey; color: black;" type="submit" class="btn subs_btn form-control">Ödemeyi yap</button>
                    </form>
                    <div class="card">
                        <div class="card-header" role="tab" id="headingThree">
                            <h5 class="mb-0">
                                <a class="collapsed btn subs_btn" data-toggle="collapse" href="#collapseThree" role="button" aria-expanded="false" aria-controls="collapseThree">
                                    Havale İle Öde
                                </a>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                Ödemenizi Havale ile gerçekleştirmek için<br>
                                AYHAN GEZİCİ - HALKBANK<br>
                                TR760001200948000001031911 <br>
                                hesabına seçtiğiniz ürünlerin ücretini, İsim Soyisiminizle birlikte aktarınız.
                                Daha sonra ise Whatsapp hattımızdan, sipariş ettiğiniz ürünleri adet sayıları ile birlikte iletiniz.
                            </div>
                        </div>
                    </div>
                   </div>
                </div>
            </div>
        </div>
     </div>
  </div>

</section>

<!--================End Register Area =================-->

<!--================Footer Area =================-->
@include('karsmutfagi.partials.footer')


<!---------  Navbar required assets --------------->
<script src="/karsmutfagi-style/js/jquery-3.3.1.min.js"></script>
<script src="/karsmutfagi-style/js/jquery.slicknav.js"></script>
<script src="/karsmutfagi-style/js/main.js"></script>
<!------------------------->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-3.2.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- Rev slider js -->
<script src="vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script src="vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<!-- Extra plugin css -->
<script src="vendors/counterup/jquery.waypoints.min.js"></script>
<script src="vendors/counterup/jquery.counterup.min.js"></script>
<script src="vendors/owl-carousel/owl.carousel.min.js"></script>
<script src="vendors/bootstrap-selector/js/bootstrap-select.min.js"></script>
<script src="vendors/image-dropdown/jquery.dd.min.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="vendors/isotope/imagesloaded.pkgd.min.js"></script>
<script src="vendors/isotope/isotope.pkgd.min.js"></script>
<script src="vendors/magnify-popup/jquery.magnific-popup.min.js"></script>
<script src="vendors/vertical-slider/js/jQuery.verticalCarousel.js"></script>
<script src="vendors/jquery-ui/jquery-ui.js"></script>

<script src="js/theme.js"></script>
</body>
</html>
