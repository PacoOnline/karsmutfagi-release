<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="/img/fazifavicon2.ico" rel="shortcut icon"/>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Şal Tokası</title>

    <!-- Icon css link -->
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/vendors/line-icon/css/simple-line-icons.css" rel="stylesheet">
    <link href="/vendors/elegant-icon/style.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Rev slider css -->
    <link href="/vendors/revolution/css/settings.css" rel="stylesheet">
    <link href="/vendors/revolution/css/layers.css" rel="stylesheet">
    <link href="/vendors/revolution/css/navigation.css" rel="stylesheet">

    <!-- Extra plugin css -->
    <link href="/vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="/vendors/bootstrap-selector/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="/vendors/vertical-slider/css/jQuery.verticalCarousel.css" rel="stylesheet">

    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

    <body
     class="home_sidebar">
        <div class="home_box">
            <!--================Menu Area =================-->
            @include('partials.nav')
             <!--================End Menu Area =================-->
            <!--================Main Content Area =================-->
            <section class="home_sidebar_area">
                <div class="container">
                    <div class="row row_disable">
                <form  action='/search' method="GET">
                        <div class="col-lg-9 float-md-right">
                            <div class="sidebar_main_content_area">
                                <div class="advanced_search_area">
                                    <select class="selectpicker">
                                     @foreach($CategoryLists as $CategoryList)
                                     <option>{{$CategoryList->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="input-group">
                                        <input type="search" class="form-control" name="search" placeholder="Kategorisinde Ara" aria-label="Search">
                                        <span class="input-group-btn">
                                            <button class="btn btn-secondary" type="submit">
                                            <i class="icon-magnifier icons"></i>
                                        </button>
                                        </span>
                                     </form>
                                     </div>

                                </div>
                                <div class="main_slider_area">

                                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                            <div class="carousel-inner">
                                                @foreach($FeaturedHomeImages as $FeaturedHomeImage)
                                                <div class="carousel-item active">
                                                    <img class="d-block w-100" style="height: 300px;" src="storage/{{$FeaturedHomeImage->first_slider_image}}" alt="First slide">

                                                </div>
                                                @endforeach

                                            @foreach($SliderImages as $SliderImage)
                                                <div class="carousel-item">
                                                    <img height=300px class="d-block w-100" src="storage/{{$SliderImage->image}}" alt="{{$SliderImage->titleofimage}}">
                                                </div>
                                                @endforeach
                                            </div>
                                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>

                                <div class="promotion_area">
                                    <div class="feature_inner row">
                                        @foreach($FeaturedHomeImages as $FeaturedHomeImage)
                                           <div class="left_promotion">
                                            <div class="f_add_item">
                                                <!-- Geniş resim sol tarafa gelecek-->
                                                <div  class="f_add_img"><img class="img-fluid" style="height: 500px;" src="/storage/{{$FeaturedHomeImage->image_left}}" alt=""></div>
                                                <div class="f_add_hover">
                                                    <h4><a style="color:white">{{$FeaturedHomeImage->title_left}}</a></h4>
                                                    <a class="add_btn" style="color:white" href="{{$FeaturedHomeImage->imgleft_product_slug}}">Şimdi incele<i class="arrow_right"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="right_promotion">
                                            <div class="f_add_item right_dir">
                                                <!-- Dar resim sağ tarafa gelecek-->
                                                <div  class="f_add_img"><img class="img-fluid" style="height: 500px;" src="/storage/{{$FeaturedHomeImage->image_right}}" alt=""></div>
                                                <div class="f_add_hover">
                                                    <h4><a style="color:white">{{$FeaturedHomeImage->title_right}}</a></h4>
                                                    <a class="add_btn" style="color:white" href="{{$FeaturedHomeImage->imgright_product_slug}}">Şimdi İncele<i class="arrow_right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                <div class="fillter_home_sidebar">
                                    <ul class="portfolio_filter">
                                        <li class="active"><a>TÜM KATEGORİLER</a></li>
                                    </ul>
                                    <div class="home_l_product_slider owl-carousel">
                                     @foreach($MaleProductLists as $MaleProductList)
                                     <div class="item woman shoes">
                                            <div class="l_product_item">
                                                <div class="l_p_img">
                                                    <img href="/urunler/{{$MaleProductList->slug}}" width=270px height=320px src="storage/{{$MaleProductList->cover_photo}}" alt="">
                                                </div>
                                                <div class="l_p_text">
                                                    <ul>
                                                        <li class="p_icon"><a href="#"><i class="icon_piechart"></i></a></li>
                                                        <li><a class="add_cart_btn" href="urun/{{$MaleProductList->slug}}">Ürüne git</a></li>
                                                        <li class="p_icon"><a href="#"><i class="icon_heart_alt"></i></a></li>
                                                    </ul>
                                                    <h4 href="/urunler/{{$MaleProductList->slug}}">{{$MaleProductList->short_description}}</h4>
                                                    <h5>{{$MaleProductList->price}}TL</h5>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        @foreach($FemaleProductLists as $FemaleProductList)
                                        <div class="item woman shoes">
                                            <div class="l_product_item">
                                                <div class="l_p_img">
                                                    <img width=270px height=320px src="storage/{{$FemaleProductList->cover_photo}}" alt="">
                                                </div>
                                                <div class="l_p_text">
                                                   <ul>
                                                        <li class="p_icon"><a href="#"><i class="icon_piechart"></i></a></li>
                                                        <li><a class="add_cart_btn" href="urun/{{$FemaleProductList->slug}}">Ürüne git</a></li>
                                                        <li class="p_icon"><a href="#"><i class="icon_heart_alt"></i></a></li>
                                                    </ul>
                                                    <h4>{{$FemaleProductList->short_description}}</h4>
                                                    <h5>{{$FemaleProductList->price}}TL</h5>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        @foreach($AccessoryProductLists as $AccessoryProductList)
                                        <div class="item shoes bags">
                                            <div class="l_product_item">
                                                <div class="l_p_img">
                                                    <img width=270px height=320px src="storage/{{$AccessoryProductList->cover_photo}}" alt="">
                                                </div>
                                                <div class="l_p_text">
                                                   <ul>
                                                        <li class="p_icon"><a href="#"><i class="icon_piechart"></i></a></li>
                                                        <li><a class="add_cart_btn" href="urun/{{$AccessoryProductList->slug}}">Ürüne git</a></li>
                                                        <li class="p_icon"><a href="#"><i class="icon_heart_alt"></i></a></li>
                                                    </ul>
                                                    <h4>{{$AccessoryProductList->short_description}}</h4>
                                                    <h5>{{$AccessoryProductList->price}}TL</h5>
                                                </div>
                                            </div>
                                        @endforeach
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                         <div class="col-lg-3 float-md-right">
                            <div class="left_sidebar_area">
                                <aside class="l_widget l_categories_widget">
                                    <div class="l_title">
                                        <h3>Kategoriler</h3>
                                    </div>
                                    <ul>
                                        @foreach($CategoryLists as $CategoryList)
                                        <li><a href="kategori/{{$CategoryList->slug}}">{{$CategoryList->name}}</a></li>
                                        @endforeach
                                    </ul>
                                </aside>
                                <aside class="l_widget l_supper_widget">
                                    @foreach($FeaturedHomeImages as $FeaturedHomeImage)
                                     <div class="f_add_item right_dir">
                                            <!-- Dar resim sağ tarafa gelecek-->
                                            <div  class="f_add_img"><img class="img-fluid" style="height: 300px;" src="/storage/{{$FeaturedHomeImage->long_image}}" alt=""></div>
                                            <div class="f_add_hover">
                                                <a class="add_btn" style="color:white" href="{{$FeaturedHomeImage->imglong_product_slug}}">Şimdi İncele<i class="arrow_right"></i></a>
                                            </div>
                                        </div>
                                    @endforeach
                                </aside>
                                <aside class="l_widget l_feature_widget">
                                    <div class="verticalCarousel">
                                        <div class="verticalCarouselHeader">
                                            <div class="float-md-left">
                                                <h3>ÖNE ÇIKAN ÜRÜNLER</h3>
                                            </div>
                                            <div class="float-md-right">
                                                <a href="#" class="vc_goUp"><i class="arrow_carrot-left"></i></a>
                                                <a href="#" class="vc_goDown"><i class="arrow_carrot-right"></i></a>
                                            </div>
                                        </div>
                                         <ul class="verticalCarouselGroup vc_list">
                                            @foreach($FeaturedProductLists as $FeaturedProductList)
                                                <li>
                                                <div class="media">
                                                    <div class="d-flex">
                                                        <img href="{{$FeaturedProductList->slug}}" height=100px width=70px src="storage/{{$FeaturedProductList->cover_photo}}" alt="">
                                                    </div>
                                                    <div class="media-body">
                                                        <h4><a style="color:black" href="urun/{{$FeaturedProductList->slug}}">{{$FeaturedProductList->short_description}}</a></h4>
                                                        <h5>{{$FeaturedProductList->price}}TL</h5>
                                                    </div>
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </aside>
                                <aside class="l_widget l_news_widget">
                                    <h3>Bültenimize abone ol</h3>
                                    <p></p>
                                    <div class="input-group">
                                        <input type="email" class="form-control" placeholder="isminiz@gmail.com" aria-label="Search for...">
                                        <span class="input-group-btn">
                                            <button class="btn btn-secondary subs_btn" type="button">Abone ol</button>
                                        </span>
                                    </div>
                                </aside>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="world_service">
                <div class="container">
                    <div class="world_service_inner">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="world_service_item">
                                    <h4><img src="img/icon/world-icon-1.png" alt="">TÜRKİYE GENELİNE HİZMET</h4>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="world_service_item">
                                    <h4><img src="img/icon/world-icon-2.png" alt="">7/24 CANLI DESTEK</h4>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="world_service_item">
                                    <h4><img src="img/icon/world-icon-3.png" alt="">GÜVENLİ ÖDEME</h4>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="world_service_item">
                                    <h4><img src="img/icon/world-icon-4.png" alt="">AYNI GÜN KARGO</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <!--================End World Wide Service Area =================-->
        <!--================Footer Area =================-->
            @include('partials.footer')

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js" integrity="sha512-jNDtFf7qgU0eH/+Z42FG4fw3w7DM/9zbgNPe3wfJlCylVDTT3IgKW5r92Vy9IHa6U50vyMz5gRByIu4YIXFtaQ==" crossorigin="anonymous"></script>

        <script>
            $(document).ready(function){
                $("img").lazyload();
            }
        </script>
        <script src="js/jquery-3.2.1.min.js"></script>

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <!-- Rev slider js -->
        <script src="vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <!-- Extra plugin css -->
        <script src="vendors/counterup/jquery.waypoints.min.js"></script>
        <script src="vendors/counterup/jquery.counterup.min.js"></script>
        <script src="vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="vendors/bootstrap-selector/js/bootstrap-select.min.js"></script>
        <script src="vendors/image-dropdown/jquery.dd.min.js"></script>
        <script src="js/smoothscroll.js"></script>
        <script src="vendors/isotope/imagesloaded.pkgd.min.js"></script>
        <script src="vendors/isotope/isotope.pkgd.min.js"></script>
        <script src="vendors/magnify-popup/jquery.magnific-popup.min.js"></script>
        <script src="vendors/vertical-slider/js/jQuery.verticalCarousel.js"></script>
        <script src="vendors/jquery-ui/jquery-ui.js"></script>
        <script src="js/theme.js"></script>

    </body>
</html>
