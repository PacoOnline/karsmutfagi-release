<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179398468-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179398468-1');
        </script>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="google-site-verification" content="F_KzXvTr_MFUBKvR-a1mO58Eix9jifaTq9oGDy8Uaps" />

         @include('karsmutfagi.partials.favicon')
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Kars Mutfağı - Sipariş Takip</title>

        <!-- Icon css link -->
        <link href="/css/font-awesome.min.css" rel="stylesheet">
        <link href="/vendors/line-icon/css/simple-line-icons.css" rel="stylesheet">
        <link href="/vendors/elegant-icon/style.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="/css/bootstrap.min.css" rel="stylesheet">

        <!-- Rev slider css -->
        <link href="/vendors/revolution/css/settings.css" rel="stylesheet">
        <link href="/vendors/revolution/css/layers.css" rel="stylesheet">
        <link href="/vendors/revolution/css/navigation.css" rel="stylesheet">

        <!-- Extra plugin css -->
        <link href="/vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
        <link href="/vendors/bootstrap-selector/css/bootstrap-select.min.css" rel="stylesheet">
        <link href="/vendors/jquery-ui/jquery-ui.css" rel="stylesheet">

        <link href="/css/style.css" rel="stylesheet">
        <link href="/css/responsive.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
        <!-- Css Styles -->
        <link rel="stylesheet" href="/karsmutfagi-style/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="/karsmutfagi-style/css/themify-icons.css" type="text/css">
        <link rel="stylesheet" href="/karsmutfagi-style/css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="/karsmutfagi-style/css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="/karsmutfagi-style/css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="/karsmutfagi-style/css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="/karsmutfagi-style/css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="/karsmutfagi-style/css/style.css" type="text/css">

    </head>
    <body>

        <!--================Menu Area =================-->
 @include('karsmutfagi.partials.header')
        <!--================End Menu Area =================-->

        <!--================Categories Banner Area =================-->
        <section class="solid_banner_area">
            <div class="container">
                <div class="solid_banner_inner">
                    <h3>Sipariş Takip</h3>
                    <ul>
                        <li><a>Anasayfa</a></li>
                        <li><a>Sipariş Takip</a></li>
                    </ul>
                </div>
            </div>
        </section>
        <!--================End Categories Banner Area =================-->

        <!--================Track Area =================-->
        <section class="track_area p_100">
            <div class="container">
                <div class="track_inner">
                    <div class="track_title">
                        <h2>Siparişin Durumunu Sorgula</h2>
                    </div>
                    <form class="track_form row">
                        <div class="col-lg-12 form-group">
                            <label for="text">Sipariş Numarası</label>
                            <input class="form-control" type="text" id="text">
                        </div>
                        <div class="col-lg-12 form-group">
                            <label for="email">Siparişte kullanılan mail adresi</label>
                            <input class="form-control" type="email" id="email">
                        </div>
                        <div class="col-lg-12 form-group">
                            <button type="submit" value="submit" class="btn subs_btn form-control">Ara</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!--================End Track Area =================-->

        <!--================Footer Area =================-->
        @include('karsmutfagi.partials.footer')


        <!---------  Navbar required assets --------------->
        <script src="/karsmutfagi-style/js/jquery-3.3.1.min.js"></script>
        <script src="/karsmutfagi-style/js/jquery.slicknav.js"></script>
        <script src="/karsmutfagi-style/js/main.js"></script>
        <!------------------------->

        <!--================End Footer Area =================-->




        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="/js/jquery-3.2.1.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="/js/popper.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <!-- Rev slider js -->
        <script src="/vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="/vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="/  vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="/vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script src="/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="/vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <!-- Extra plugin css -->
        <script src="/vendors/counterup/jquery.waypoints.min.js"></script>
        <script src="/vendors/counterup/jquery.counterup.min.js"></script>
        <script src="/vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="/vendors/bootstrap-selector/js/bootstrap-select.min.js"></script>
        <script src="/vendors/image-dropdown/jquery.dd.min.js"></script>
        <script src="/js/smoothscroll.js"></script>
        <script src="/vendors/isotope/imagesloaded.pkgd.min.js"></script>
        <script src="/vendors/isotope/isotope.pkgd.min.js"></script>
        <script src="/vendors/magnify-popup/jquery.magnific-popup.min.js"></script>
        <script src="/vendors/vertical-slider/js/jQuery.verticalCarousel.js"></script>
        <script src="/vendors/jquery-ui/jquery-ui.js"></script>

        <script src="/js/theme.js"></script>
    </body>
</html>
