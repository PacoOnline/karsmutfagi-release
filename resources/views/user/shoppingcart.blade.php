<!DOCTYPE html>
<html lang="en">
<head>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-179398468-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-179398468-1');
</script>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

@include('karsmutfagi.partials.favicon')
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Kars Mutfağı-Sepetim</title>

<meta name="csrf-token" content="{{ csrf_token() }}">

<style>

    .quantity {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        margin-bottom: 40px;
    }

    .quantity p {
        float: left;
        margin-right: 15px;
        text-transform: uppercase;
        font-weight: 700;
        color: #414141;
        padding-top: 10px;
        margin-bottom: 0;
    }

    .quantity .pro-qty {
        width: 94px;
        height: 36px;
        border: 1px solid #ddd;
        padding: 0 15px;
        border-radius: 40px;
        float: left;
    }

    .quantity .pro-qty .qtybtn {
        width: 15px;
        display: block;
        float: left;
        line-height: 36px;
        cursor: pointer;
        text-align: center;
        font-size: 18px;
        color: #404040;
    }

    .quantity .pro-qty input {
        width: 18px;
        float: left;
        border: none;
        height: 36px;
        line-height: 40px;
        padding: -10px;
        font-size: 14px;
        text-align: center;
        background-color: transparent;
    }

    /* -- quantity box -- */

    .quantity {
        display: inline-block; }

    .quantity .input-text.qty {
        width: 35px;
        height: 39px;
        padding: 0 5px;
        text-align: center;
        background-color: transparent;
        border: 1px solid #efefef;
    }

    .quantity.buttons_added {
        text-align: left;
        position: relative;
        white-space: nowrap;
        vertical-align: top; }

    .quantity.buttons_added input {
        display: inline-block;
        margin: 0;
        vertical-align: top;
        box-shadow: none;
    }

    .quantity.buttons_added .minus,
    .quantity.buttons_added .plus {
        padding: 7px 10px 8px;
        height: 41px;
        background-color: #ffffff;
        border: 1px solid #efefef;
        cursor:pointer;}

    .quantity.buttons_added .minus {
        border-right: 0; }

    .quantity.buttons_added .plus {
        border-left: 0; }

    .quantity.buttons_added .minus:hover,
    .quantity.buttons_added .plus:hover {
        background: #eeeeee; }

    .quantity input::-webkit-outer-spin-button,
    .quantity input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        -moz-appearance: none;
        margin: 0; }

    .quantity.buttons_added .minus:focus,
    .quantity.buttons_added .plus:focus {
        outline: none; }




</style>

    <!-- Icon css link -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="vendors/line-icon/css/simple-line-icons.css" rel="stylesheet">
    <link href="vendors/elegant-icon/style.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Rev slider css -->
    <link href="vendors/revolution/css/settings.css" rel="stylesheet">
    <link href="vendors/revolution/css/layers.css" rel="stylesheet">
    <link href="vendors/revolution/css/navigation.css" rel="stylesheet">

    <!-- Extra plugin css -->
    <link href="vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="vendors/bootstrap-selector/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="vendors/jquery-ui/jquery-ui.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!-- HTML5 shim an  d Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <!-- Css Styles -->
    <link rel="stylesheet" href="/karsmutfagi-style/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/themify-icons.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/style.css" type="text/css">
<script data-require="jquery@3.1.1" data-semver="3.1.1" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>

<!--================Menu Area =================-->
<!--================End Menu Area =================-->
@include('karsmutfagi.partials.header')
<!--================Categories Banner Area =================-->
<section class="solid_banner_area">
<div class="container">
<div class="solid_banner_inner">
    <h3>Alışveriş Sepetim</h3>
    <ul>
        <li><a href="/">Anasayfa</a></li>
        <li><a>Alışveriş Sepeti</a></li>
    </ul>
</div>
</div>
</section>
<!--================End Categories Banner Area =================-->
@include('partials.errors')
@if (session()->has('success'))
<section class="shopping_cart_area mt-5">

<div class="container">
    <div class="col-lg-12">
        <div class="alert alert-success" role="alert">
            Ürününüz Sepete Başarıyla Eklendi
        </div>
    </div>
</div>
</section>
@endif
<!--================Shopping Cart Area =================-->
@if(count(Cart::content())>0)
<section class="shopping_cart_area p_100">
<div class="container">
<div class="row">
    <div class="col-lg-8">
        <div class="cart_items">
            <h3>Sepetinizdeki Ürünler</h3>
            <div class="table-responsive-md">
                <table class="table">
                    <tbody>
                        @foreach(Cart::content() as $urunCartItem)
                            <th scope="row">
                             <!----
                              <form method="post"  action="{{route('sepet.kaldir', $urunCartItem->rowId)}}">
                               @csrf
                                {{method_field('DELETE')}}
                                <input type="image" src="img/icon/close-icon.png" value="Sepetten kaldır">
                              </form>
                              ----->
                            </th>
                            <td>
                                <div class="media">
                                    <div class="d-flex">
                                        <img href="/urun/{{$urunCartItem->slug}}" height="128px" width="128px" src="/storage/{{ $urunCartItem->options->cover_photo }}" alt="">
                                    </div>
                                    <div class="media-body">
                                        <a style="color: #0b0b0b" href="/urun/{{$urunCartItem->slug}}">{{ $urunCartItem->name }}</a>
                                    </div>
                                </div>
                            </td>
                            <td><p class="red"></p></td>

                            <td>
                                <form method="post" action="{{route('sepet.guncelle',$urunCartItem->rowId)}}">
                                    @csrf
                                    <input type="hidden" name="_method" value="post">
                                    <div class="quantity buttons_added">
                                    <input type="button" value="-" class="minus">
                                    <input type="number" step="1" min="1" max="" name="quantity" value="{{$urunCartItem->qty}}" title="Qty" class="input-text qty text" size="4" pattern="" inputmode="">
                                    <input type="button" value="+" class="plus">
                                </div>
                                    <td>
                                    <input type="submit" class="btn btn-dark" value="Adet Güncelle">
                                    </td>
                                </form>
                            </td>
                            <td><p style="margin-top: 12px; color: red;">{{ $urunCartItem->subtotal }}₺</p></td>
                        </tr>
                        @endforeach
                        <tr class="last">
                            <th scope="row">
                                <img src="img/icon/cart-icon.png" alt="">
                            </th>
                            <td>
                        <div class="media">
                        <div class="d-flex">
                            <h5>Kupon Kodunuz</h5>
                            </div>
                            <div class="media-body">
                             <input type="text" placeholder="Kupon uygula">
                            </div>
                                </div>
                            </td>
                            <td><p class="red"></p></td>
                            <td>
                                <form method="post" action="{{ route('sepet.bosalt') }}">
                                @csrf
                                    {{ method_field('DELETE') }}
                                    <input class="btn btn-danger" type="submit" value="Tüm Sepeti boşalt">
                                </form>
                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="cart_totals_area">
            <h4>Ödenecek Tutar</h4>
            <div class="cart_t_list">
                <div class="media">
                    <div class="d-flex">
                        <h5>Ürün tutarları</h5>
                    </div>
                <div class="media-body">
                    <h6> &nbsp; {{ Cart::subtotal() }} ₺</h6>
                </div>
                </div>

            </div>
            <div class="total_amount row m0 row_disable">
                <div class="float-left">
                    Toplam
                </div>
                <div class="float-right">
                  &nbsp;  {{Cart::subtotal()}}  ₺
                </div>
            </div>
        </div>
        <button style="background-color: grey; color: white;" type="submit" onclick="window.location.href='/odeme'" value="submit" class="btn subs_btn form-control">Ödemeye devam et</button>
    </div>
</div>
</div>
</section>
<!--================End Shopping Cart Area =================-->
@else
<section class="emty_cart_area p_100">
<div class="container">
    <div class="emty_cart_inner">
        <i class="icon-handbag icons"></i>
        <h3>Sepetiniz boş</h3>
        <h4>Alışverişe <a href="{{ route('anasayfa') }}">Geri Dön</a></h4>
    </div>
</div>
</section>
@endif

@section('footer')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.urun-adet-artir, .urun-adet-azalt').on('click', function () {

        var id = $(this).attr('data-id');
        var adet = $(this).attr('data-adet');

        $.ajax({
            type: 'PATCH',
            url : '/sepet/guncelle/' + id,
            data: {adet: adet, _method: 'PATCH'},
            success : function () {
                window.location.href = '/sepet';
            }
        });
    });
</script>
@endsection

<!--================Footer Area =================-->
@include('karsmutfagi.partials.footer')



<!---------  Navbar required assets --------------->
<script src="/karsmutfagi-style/js/jquery-3.3.1.min.js"></script>
<script src="/karsmutfagi-style/js/jquery.slicknav.js"></script>
<script src="/karsmutfagi-style/js/main.js"></script>
<!------------------------->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/js/jquery-3.2.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/js/popper.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<!-- Rev slider js -->
<script src="/vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="/vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="/vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="/vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script src="/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="/vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<!-- Extra plugin css -->
<script src="/vendors/counterup/jquery.waypoints.min.js"></script>
<script src="/vendors/counterup/jquery.counterup.min.js"></script>
<script src="/vendors/owl-carousel/owl.carousel.min.js"></script>
<script src="/vendors/bootstrap-selector/js/bootstrap-select.min.js"></script>
<script src="/vendors/image-dropdown/jquery.dd.min.js"></script>
<script src="/js/smoothscroll.js"></script>
<script src="/vendors/isotope/imagesloaded.pkgd.min.js"></script>
<script src="/vendors/isotope/isotope.pkgd.min.js"></script>
<script src="/vendors/magnify-popup/jquery.magnific-popup.min.js"></script>
<script src="/vendors/vertical-slider/js/jQuery.verticalCarousel.js"></script>
<script src="/vendors/jquery-ui/jquery-ui.js"></script>

<script src="/js/theme.js"></script>
<script>
    function wcqib_refresh_quantity_increments() {
        jQuery("div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)").each(function(a, b) {
            var c = jQuery(b);
            c.addClass("buttons_added"), c.children().first().before('<input type="button" value="-" class="minus" />'), c.children().last().after('<input type="button" value="+" class="plus" />')
        })
    }
    String.prototype.getDecimals || (String.prototype.getDecimals = function() {
        var a = this,
            b = ("" + a).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
        return b ? Math.max(0, (b[1] ? b[1].length : 0) - (b[2] ? +b[2] : 0)) : 0
    }), jQuery(document).ready(function() {
        wcqib_refresh_quantity_increments()
    }), jQuery(document).on("updated_wc_div", function() {
        wcqib_refresh_quantity_increments()
    }), jQuery(document).on("click", ".plus, .minus", function() {
        var a = jQuery(this).closest(".quantity").find(".qty"),
            b = parseFloat(a.val()),
            c = parseFloat(a.attr("max")),
            d = parseFloat(a.attr("min")),
            e = a.attr("step");
        b && "" !== b && "NaN" !== b || (b = 0), "" !== c && "NaN" !== c || (c = ""), "" !== d && "NaN" !== d || (d = 0), "any" !== e && "" !== e && void 0 !== e && "NaN" !== parseFloat(e) || (e = 1), jQuery(this).is(".plus") ? c && b >= c ? a.val(c) : a.val((b + parseFloat(e)).toFixed(e.getDecimals())) : d && b <= d ? a.val(d) : b > 0 && a.val((b - parseFloat(e)).toFixed(e.getDecimals())), a.trigger("change")
    });
</script>
</body>
</html>
