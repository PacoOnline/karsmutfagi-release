<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

@include('karsmutfagi.partials.favicon')

<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Kars Mutfağı - Siparişlerim</title>

    <!-- Icon css link -->
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/vendors/line-icon/css/simple-line-icons.css" rel="stylesheet">
    <link href="/vendors/elegant-icon/style.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Rev slider css -->
    <link href="/vendors/revolution/css/settings.css" rel="stylesheet">
    <link href="/vendors/revolution/css/layers.css" rel="stylesheet">
    <link href="/vendors/revolution/css/navigation.css" rel="stylesheet">

    <!-- Extra plugin css -->
    <link href="/vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="/vendors/bootstrap-selector/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="/vendors/jquery-ui/jquery-ui.css" rel="stylesheet">

    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="/karsmutfagi-style/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/themify-icons.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="/karsmutfagi-style/css/style.css" type="text/css">

</head>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>
<body>

<!--================Menu Area =================-->
<!--================End Menu Area =================-->
<style>
    body {
        font-family: Arial, sans-serif;
        font-size: 14px;
        line-height: 20px;
        color: #333333;
    }

    table, th, td {
        border: solid 1px #000;
        padding: 10px;
    }

    table {
        border-collapse:collapse;
        caption-side:bottom;
    }

    caption {
        font-size: 16px;
        font-weight: bold;
        padding-top: 5px;
    }

</style>
@include('karsmutfagi.partials.header')

<!--================Categories Banner Area =================-->
<section class="solid_banner_area">
    <div class="container">
        <div class="solid_banner_inner">
            <h3>SİPARİŞLERİM</h3>
            <ul>
                <li><a>Anasayfa</a></li>
                <li><a>Siparişlerim</a></li>
            </ul>
        </div>
    </div>
</section>
<!--================End Categories Banner Area =================-->
<!--================Shopping Cart Area =================-->
<section class="shopping_cart_area p_100">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="cart_items">
                    <div class="table-responsive-md">

                        <h3>Siparişin Detayları </h3>
                         <div class="container">
                            <div class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                             <a data-toggle="collapse" style="color:#9f9fa0;" href="#collapse1">{{$orderDetails->created_at->format('d.m.Y')}} tarihli siparişin detayları</a> &nbsp;<i class="fa fa-arrow-down" aria-hidden="true"></i>
                                        </h4>
                                    </div>
                                     <div id="collapse1"  class="panel-collapse collapse">
                                        <table>
                                            <thead>
                                            <tr>
                                                <th>&nbsp;</th>
                                               <th><a>Sipariş Bilgileri</a></th>
                                            </tr>
                                            </thead>
                                            <tr>
                                            <tr>
                                                <td>İsim- Soyisim</td>
                                                <td>{{$orderDetails->name}}</td>

                                            </tr>
                                            <tr>
                                                <td>Şehir</td>
                                                <td>{{$orderDetails->city}}</td>

                                            </tr>
                                            <tr>
                                                <td>Adres</td>
                                                <td>{{$orderDetails->address}}</td>

                                            </tr>
                                            <tr>
                                                <td>Email adresi</td>
                                                <td>{{$orderDetails->email}}</td>

                                            </tr>
                                            <tr>
                                                <td>Telefon Numarası</td>
                                                <td>{{$orderDetails->phonenumber}}</td>
                                            </tr>

                                           <!---
                                            <tr>
                                                <td>Sipariş Fiyati</td>
                                                <td>{{$orderDetails->price}} TL</td>
                                            </tr>
                                           -->
                                        @if(isset($orderDetails->notes))

                                                <tr>
                                                    <td>Sipariş Notları</td>
                                                    <td>{{$orderDetails->notes}}</td>
                                                </tr>

                                        @endif
                                            <tr>
                                                <td>Sipariş Tarihi</td>
                                                <td> {{$orderDetails->created_at }}  </td>
                                            </tr>

                                            <tr>
                                                <td><strong>Sipariş Durumu</strong></td>
                                                <td>{{$orderDetails->status}}</td>
                                            </tr>

                                            <tr>
                                            <td>
                                                <strong><h4>Sipariş Edilmiş Ürünler</h4></strong>
                                            </td>
                                            </tr>
                                            @foreach($products as $key => $product)
                                            <tr>
                                                <td><strong><img class="media-object" style="height: 200px; width: 250px;" src="/storage/{{$product->cover_photo}}"></strong></td>
                                                <td>
                                                <strong>Ürün İsmi</strong><h5>{{$product->short_description}}</h5> <br>
                                                    <strong>Ürün Fiyatı</strong>    <h5>{{$product->price}}₺</h5> <br>
                                                    <strong>Ürün Adeti</strong> <h5>{{$product->quantity}}</h5><br>
                                                    <strong>Ürün Kodu</strong> <h5>{{$product->product_code}}</h5>
                                                </td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
               </div>
            </div>
             </div>

    </div>
    </div>

</section>
@include('karsmutfagi.partials.footer')
<!---------  Navbar required assets --------------->
<script src="/karsmutfagi-style/js/jquery-3.3.1.min.js"></script>
<script src="/karsmutfagi-style/js/jquery.slicknav.js"></script>
<script src="/karsmutfagi-style/js/main.js"></script>
<!------------------------->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/js/jquery-3.2.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/js/popper.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<!-- Rev slider js -->
<script src="/vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="/vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="/vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="/vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script src="/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="/vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<!-- Extra plugin css -->
<script src="/vendors/counterup/jquery.waypoints.min.js"></script>
<script src="/vendors/counterup/jquery.counterup.min.js"></script>
<script src="/vendors/owl-carousel/owl.carousel.min.js"></script>
<script src="/vendors/bootstrap-selector/js/bootstrap-select.min.js"></script>
<script src="/vendors/image-dropdown/jquery.dd.min.js"></script>
<script src="/js/smoothscroll.js"></script>
<script src="/vendors/isotope/imagesloaded.pkgd.min.js"></script>
<script src="/vendors/isotope/isotope.pkgd.min.js"></script>
<script src="/vendors/magnify-popup/jquery.magnific-popup.min.js"></script>
<script src="/vendors/vertical-slider/js/jQuery.verticalCarousel.js"></script>
<script src="/vendors/jquery-ui/jquery-ui.js"></script>

<script src="/js/theme.js"></script>
</body>
</html>
