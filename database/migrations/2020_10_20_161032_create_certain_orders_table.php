<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertainOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certain_orders' , function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->string('name', 20);
            $table->string('surname', 20);
            $table->string('city',10);
            $table->string('address',400);
            $table->string('email');
            $table->string('phonenumber');
            $table->string('identitynumber');
            $table->string('notes')->nullable();
            $table->integer('sepet_id')->unsigned();
            $table->decimal('price', 5,4);
            $table->string('status', 50)->nullable();

            $table->foreign('sepet_id')->references('id')->on('shopping_card')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certain_orders');
    }
}
