<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShoppingCardProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping_card_product', function (Blueprint $table) {
            $table->Increments('id');
            $table->Integer('sepet_id')->unsigned();
            $table->Integer('urun_id')->unsigned();
            $table->integer('quantity');
            $table->decimal('price', 5,2);
            $table->string('status', 30);
            $table->timestamps();
            $table->softDeletes('deleted_at');

            $table->foreign('sepet_id')->references('id')->on('shopping_card')->onDelete('cascade');
            $table->foreign('urun_id')->references('id')->on('products')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopping_card_product');
    }
}
